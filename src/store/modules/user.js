import { login, logout, getInfo } from '@/api/user'
import { setUserInfo, getUserInfo, delUserInfo } from '@/utils/userInfo'
import router from '@/router'
import { getToken, setToken, removeToken } from '@/utils/auth'
const state = {
  token: getToken(),
  userInfo: getUserInfo()
}
const mutations = {
  //
  setToken(state, token) {
    state.token = token
    setToken(token)
  },
  //
  setUserInfo(state, info) {
    state.userInfo = info
    setUserInfo(info)
  },
  //
  logout(state) {
    state.token = ''
    state.userInfo = ''
    removeToken()
    delUserInfo()
    router.push('/login')
  }
}
const actions = {
  // 登录
  async loginAsync(store, data) {
    const res = await login(data)
    store.commit('setToken', res.token)
  },
  // 退出
  async logoutAsync(store) {
    await logout()
    store.commit('logout')
  },
  // 获取用户信息
  async getUserInfo(store) {
    const res = await getInfo()
    store.commit('setUserInfo', res)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}

