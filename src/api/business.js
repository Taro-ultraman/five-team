import request from '@/utils/request'

// 获取企业列表/enterprise/list
export function getbusiness(params) {
  return request.get('/enterprise/list', { params })
}

// 搜索企业列表
export function searchbusiness(params) {
  return request.get('/enterprise/list', { params })
}

// 新增企业列表 /enterprise/add
export function addbusiness(data) {
  return request.post('/enterprise/add', data)
}

// 编辑企业列表 /enterprise/edit
export function editbusiness(data) {
  return request.post('/enterprise/edit', data)
}
// 回显数据
export function echobusiness(params) {
  return request.get(`/enterprise/list`, { params })
}

// 删除企业列表 /enterprise/remove
export function delbusiness(data) {
  return request.post(`/enterprise/remove`, data)
}

// 状态设置 /enterprise/status
export function statusbusiness(data) {
  return request.post(`/enterprise/status`, data)
}
