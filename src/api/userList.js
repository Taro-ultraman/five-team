import request from '@/utils/request'

// 获取数据
export function getUserList(params) {
  return request({
    url: '/user/list',
    params
  })
}

//  删除接口
export function deleteUser(data) {
  return request({
    url: '/user/remove',
    method: 'post',
    data
  })
}

// 新增用户
export function addUser(data) {
  return request({
    url: '/user/add',
    method: 'post',
    data
  })
}

// 修改接口
export function editUser(data) {
  return request({
    url: '/user/edit',
    method: 'post',
    data
  })
}

// 启用和禁用接口
export function userStatus(data) {
  return request({
    url: '/user/status',
    method: 'post',
    data
  })
}

