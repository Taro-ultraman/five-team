import request from '@/utils/request'
// 获取题目列表信息接口
export const getQuestionListApi = params => request.get('/question/list', { params })

// 获取学科列表信息接口
export const getSubjectListApi = () => request.get('/subject/list')

// 获取企业列表信息接口
export const getEnterpriseListApi = () => request.get('/enterprise/list')

// 上传文件
export const uploadApi = data => {
  return request({
    url: '/question/upload',
    method: 'post',
    data,
    responseType: 'blob' // 二进制文件流
  })
}
// 修改题目状态。启用或者禁用账号

export const EditStatysApi = data => request.post('/question/status', data)
// 删除题目
export const delQuestionApi = data => request.post('/question/remove', data)

// 发布题目
export const addQuestionApi = data => request.post('/question/add', data)

// 题目编辑接口
export const editQuestionApi = data => request.post('/question/edit', data)
