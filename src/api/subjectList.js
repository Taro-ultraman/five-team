import request from '@/utils/request'

// 获取学科列表/subject/list
export function getsubject(params) {
  return request.get('/subject/list', { params })
}

// 搜索学科列表
export function searchsubject(params) {
  return request.get('/subject/list', { params })
}

// 新增学科列表 /subject/add
export function addsubject(data) {
  return request.post('/subject/add', data)
}

// 编辑学科列表 /enterprise/edit
export function editsubject(data) {
  return request.post('/subject/edit', data)
}
// 删除学科列表 /enterprise/remove
export function delsubject(data) {
  return request.post(`/subject/remove`, data)
}

// 状态设置 /enterprise/status
export function statussubject(data) {
  return request.post(`/subject/status`, data)
}
