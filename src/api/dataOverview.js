import request from '@/utils/request'

export function getDetails() {
  return request({
    url: '/data/title',
    method: 'post'
  })
}

export function getEchartsData() {
  return request({
    url: '/data/statistics',
    method: 'post'
  })
}
