import request from '@/utils/request'

// 登录
export const login = (data) => {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}
// 退出
export const logout = () => {
  return request({
    url: '/logout'
  })
}
// 获取用户信息
export const getInfo = () => {
  return request({
    url: '/info'
  })
}
// 获取登录验证码
export const getImg = (params) => {
  return request({
    url: '/captcha',
    params,
    responseType: 'blob'
  })
}

// 注册
// 生成图形验证码
export const getImgCode = (params) => {
  return request({
    url: '/captcha',
    params: {
      type: 'sendsms'
    },
    responseType: 'blob'
  })
}
// 获取短信验证码
export const getRcode = (data) => {
  return request({
    url: '/sendsms',
    method: 'post',
    data
  })
}
// 提交注册
export const submitRegister = (data) => {
  return request({
    url: '/register',
    method: 'post',
    data
  })
}
