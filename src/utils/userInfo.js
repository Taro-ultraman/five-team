const User_Key = 'admin_user_key'

export function setUserInfo(info) {
  return localStorage.setItem(User_Key, JSON.stringify(info))
}

export function getUserInfo() {
  return JSON.parse(localStorage.getItem(User_Key))
}

export function delUserInfo() {
  return localStorage.removeItem(User_Key)
}
