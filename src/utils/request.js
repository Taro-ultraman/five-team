import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 30000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  (config) => {
    const token = store.getters.token
    if (token) {
      config.headers.token = token
    }
    return config
  },
  (error) => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  (response) => {
    // console.log('响应拦截器', response)
    if (response.data instanceof Blob) {
      return response.data
    } else {
      const { code, message, data } = response.data
      if (code === 200) {
        return data
      } else {
        if (store.getters.token) {
          if (code === 206) {
            store.dispatch('user/logoutAsync')
            return Message.warning('登录过期，请重新登录')
          }
        }
        Message.error(message)
        return Promise.reject(new Error(message))
      }
    }
  },
  (error) => {
    console.dir(error)
    return Promise.reject(error)
  }
)

export default service
